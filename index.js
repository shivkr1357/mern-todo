const express = require("express");

const app = express();
const dotenv = require("dotenv");
const cors = require("cors");
const morgan = require("morgan");
const todoRoutes = require("./routes/todoRoutes");

const mongoose = require("mongoose");

// middlewares

dotenv.config();

app.use(cors());
app.use(express.json());
app.use(morgan("common"));

// Routes
app.use("/api/todo", todoRoutes);

//setting up mongoose database
mongoose.set("strictQuery", true);

mongoose.connect("mongodb://localhost:27017/todo", () => {
  console.log("Connected");
});

//setting up the server for listen

app.listen(process.env.PORT || 4000, () => {
  console.log(`Server is running on the port ${process.env.PORT}`);
});
