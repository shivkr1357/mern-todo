const router = require("express").Router();
const Todo = require("../models/Todo");

router.get("/", async (req, res) => {
  const todo = await Todo.find().sort({ createdAt: "asc" });

  res.status(200).json(todo);
});

router.post("/create", async (req, res) => {
  const todo = new Todo(req.body);
  try {
    const todoNew = await todo.save();

    res
      .status(200)
      .json({ message: "Todo Created successfully", todo: todoNew });
  } catch (error) {
    res.status(500).json({ error: error });
  }
});

router.put("/update/:id", async (req, res) => {
  try {
    const todo = await Todo.findById(req.params.id);

    if (!req.body.todo) {
      await todo.updateOne({
        todo: todo.todo,
        isCompleted: req.body.isCompleted,
      });
    } else {
      await todo.updateOne({
        todo: req.body.todo,
        isCompleted: req.body.isCompleted,
      });
    }

    res.status(200).json({ message: "todo updated successfully", data: todo });
  } catch (error) {
    res.status(500).json({ error: error });
  }
});

router.delete("/delete/:id", async (req, res) => {
  try {
    await Todo.deleteOne({ _id: req.params.id });

    res.status(200).json({ message: "todo deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: error });
    res.sendStatus(200).json();
  }
});

module.exports = router;
